// header
window.onscroll = function() {Header()};

var header = document.getElementById("header");

function Header() {
  if (window.pageYOffset > 50) {
    header.classList.add("header--sticky");
  } else {
    header.classList.remove("header--sticky");
  }
}

// welcome
const w_phones = document.querySelectorAll('.w-nav__btn');

w_phones.forEach(w_phone => {
  const item_data = w_phone.dataset.phone
  const item = document.getElementById(item_data);

  console.log(item)

  w_phone.onmouseover = function() {
    item.classList.add('active')
  };

  w_phone.onmouseout = function() {
    item.classList.remove('active')
  };
});



setTimeout(function(){
  // .u-club-slider
  var uClubSlider = new Swiper(".u-club-slider", {
    centeredSlides: true,
    loop: true,
    loopAdditionalSlides: 30,
    slidesPerView : 'auto',
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  });
},100)

// .tournaments-slider
// var tournamentsSlider = new Swiper(".tournaments-slider", {
//   centeredSlides: true,
//   loop: true,
//   loopAdditionalSlides: 30,
//   slidesPerView : 'auto'
// });

var reviewSlider = new Swiper('.tournaments-slider', {
  effect: 'coverflow',
  slidesPerView: 1,
  centeredSlides: true,
  initialSlide: 1,
  clickable: true,
  loop: true,
  loopAdditionalSlides: 30,
  spaceBetween: -35,
  coverflowEffect: {
      rotate: 0,
      stretch: -55,
      depth: 130,
      modifier: 2,
      slideShadows: false,
  },
  breakpoints: {  
    '1440': {
      spaceBetween: 0
    },
  },
});

// .partners-slider
var partnersSlider = new Swiper(".partners-slider", {
  centeredSlides: true,
  loop: true,
  loopAdditionalSlides: 30,
  slidesPerView: 5,
});

// .download-slider
var downloadTrambs = new Swiper("#download-trambs", {
  centeredSlides: true,
  loop: true,
  loopAdditionalSlides: 30,
  slidesPerView : 'auto',
  touchRatio: 0.2,
  slideToClickedSlide: true,
  navigation: {
    nextEl: "#download-trambs-next",
    prevEl: "#download-trambs-prev"
  }
});

var downloadSlider = new Swiper("#download-slider", {
  loop: true,
  loopAdditionalSlides: 30,
});

downloadTrambs.controller.control = downloadSlider;
downloadSlider.controller.control = downloadTrambs;